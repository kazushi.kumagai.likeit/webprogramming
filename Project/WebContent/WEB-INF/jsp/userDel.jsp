<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>userUp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>
    <div class="alert alert-dark" role="alert" align="right">
   	${userInfo.name} さん
   	<a href="userLogout" class="navbar-link logout-link">
    <button type="button" class="btn btn-outline-danger">ログアウト</button>
    </a>
</div>

    <br>
     <div class="text-center">
    <h2>ユーザー削除確認</h2>
    </div>
<br>

    <h4>ログインID:${user.loginId}</h4>
    <h4>を本当に削除してよろしいでしょうか。</h4>

    <br>

    <div class="row col-6 mx-auto">
    <div class="col-sm-6">
    	<a href="userList" class="navbar-link logout-link">
        <button type="button" class="btn btn-primary btn-block" "col-sm-3">キャンセル</button></div>
        </a>
     <div class="col-sm-6">
     	<form action="UserDel" method="post">
     		<input type="hidden" name="id" class="form-control form-control-sm" id="colFormLabelSm" value="${user.id}" readonly >
  		   	<button type="submit" class="btn btn-secondary btn-block" "col-sm-3">OK</button>
     	</form>
         </div>
    </div>
</body>
</html>