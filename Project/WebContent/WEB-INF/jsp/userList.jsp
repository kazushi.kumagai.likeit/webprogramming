<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>userList</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <div class="alert alert-dark" role="alert" align="right">
   	${userInfo.name} さん
   	<a href="userLogout" class="navbar-link logout-link">
    <button type="button" class="btn btn-outline-danger">ログアウト</button>
    </a>
</div>


    <br>
     <div class="text-center">
    <h2>ユーザ一覧</h2>
    </div>

<div class="col-10 mx-auto">

 <div align="right">
 	<a href="UserLeg" class="navbar-link logout-link">
        <button type="button" class="btn btn-outline-primary">新規登録</button>
        </a>
    </div>

    <br>

<form action="userList" method="post">
  <div class="form-group row">
      <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">ログインID</label>
    <div class="col-sm-8">
      <input type="text" name="loginId" class="form-control form-control-sm" id="colFormLabelSm">
    </div>
  </div>
  <div class="form-group row">
      <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">ユーザID</label>
    <div class="col-sm-8">
      <input type="text" name="name" class="form-control form-control-sm" id="colFormLabelSm">
    </div>
  </div>

    <div class="form-group row">
      <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">生年月日</label>


    <div class="col-sm-4">
      <input type="date" name="startDate" class="form-control form-control-sm" id="colFormLabelSm">
    </div>
        〜
    <div class="col-sm-4">
      <input type="date" name="endDate" class="form-control form-control-sm" id="colFormLabelSm">
    </div>
  </div>

    <br>
    <div align="right">
<button type="submit" class="col-2 btn btn-outline-primary">検索</button>
    </div>

</form>
    </div>

<br>
<hr>
<br>

<table class="col-10 mx-auto table table-bordered">
  <thead>
    <tr>
      <th scope="col">ログインID</th>
      <th scope="col">ユーザ名</th>
      <th scope="col">生年月日</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
   <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->
                     <td>
                     <!-- 全てのユーザーに表示 -->
						<a href="userDetail?id=${user.id}" class="navbar-link logout-link">
							<button type="button" class="btn btn-outline-primary">参照</button>
						</a>
					<!-- 管理者、もしくはログインしたユーザーと一致する場合に表示 -->
					<c:if test= "${userInfo.loginId == 'admin' || userInfo.loginId == user.loginId}">
						<a href="UserUpdate?id=${user.id}" class="navbar-link logout-link">
							<button type="button" class="btn btn-outline-success">更新</button>
						</a>
					</c:if>
					<!-- 管理者のみ表示 -->
					<c:if test= "${userInfo.loginId == 'admin'}">
						<a href="UserDel?id=${user.id}" class="navbar-link logout-link">
							<button type="button" class="btn btn-outline-danger">削除</button>
						</a>
					</c:if>
                     </td>
                   </tr>
                 </c:forEach>

  </tbody>
</table>

</body>
</html>