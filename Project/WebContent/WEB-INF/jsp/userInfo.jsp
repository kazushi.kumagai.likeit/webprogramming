<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>userUp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>
    <div class="alert alert-dark" role="alert" align="right">
   	${userInfo.name} さん
   	<a href="userLogout" class="navbar-link logout-link">
    <button type="button" class="btn btn-outline-danger">ログアウト</button>
    </a>
</div>

    <br>
     <div class="text-center">
    <h2>ユーザー情報詳細参照</h2>
    </div>
<br>
<div class="col-10 mx-auto">

    <div class="form-group row">
        <label for="colFormLabelSm" class="col-sm-6 col-form-label col-form-label-sm"><b>ログインID</b></label>
    <div class="col-sm-6">
      ${user.loginId}
    </div>
  </div>

    <div class="form-group row">
        <label for="colFormLabelSm" class="col-sm-6 col-form-label col-form-label-sm"><b>ユーザID</b></label>
    <div class="col-sm-6">
      ${user.name}
    </div>
  </div>

    <div class="form-group row">
        <label for="colFormLabelSm" class="col-sm-6 col-form-label col-form-label-sm"><b>生年月日</b></label>
    <div class="col-sm-6">
      ${user.birthDate}
    </div>
  </div>

    <div class="form-group row">
        <label for="colFormLabelSm" class="col-sm-6 col-form-label col-form-label-sm"><b>登録日時</b></label>
    <div class="col-sm-6">
      ${user.createDate}
    </div>
  </div>

    <div class="form-group row">
        <label for="colFormLabelSm" class="col-sm-6 col-form-label col-form-label-sm"><b>更新日時</b></label>
    <div class="col-sm-6">
      ${user.updateDate}
    </div>
  </div>

  <br>

   <div>
		<a href="userList" class="navbar-link logout-link">
			<button type="button" class="btn btn-outline-primary">戻る</button>
 		</a>
    </div>
</div>
<!--
    <table class="table table-borderless">

    <tr>
        <th class="col-2">ログインID</th>
        <td class="col-6">id0001</td>
    </tr>

    <tr>
      <th class="col-6">ユーザ名</th>
      <td class="col-6">田中太郎</td>
    </tr>

    <tr>
      <th class="col-6">生年月日</th>
      <td class="col-6">1989年04月26日</td>
    </tr>

    <tr>
      <th class="col-6">登録日時</th>
      <td class="col-6">2017年01月01日 10:50</td>
    </tr>

    <tr>
      <th class="col-6">更新日時</th>
      <td class="col-6">2017年02月01日 01:05</td>
    </tr>


</table>
    </div>
-->
</body>
</html>