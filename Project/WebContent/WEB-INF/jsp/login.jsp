<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <br>
    <div class="text-center">
    <h2>ログイン画面</h2>
    </div>

    <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

<br>

    <div class="col-6 mx-auto">
    <form action="userLogin" method="post">
 		<div class="form-group row">
 			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">ログインID</label>
			<div class="col-sm-8">
			<input type="text" name="loginId" class="form-control form-control-sm" id="inputLoginId">
			</div>
		</div>
		<div class="form-group row">
			<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">パスワード</label>
			<div class="col-sm-8">
			<input type="password" name="password" class="form-control form-control-sm" id="inputPassword">
			</div>
		</div>
		<div align="center">
        	<button type="submit" class="btn btn-outline-primary">ログイン</button>
        </div>

        <br>
	</form>
    </div>



</body>

</html>