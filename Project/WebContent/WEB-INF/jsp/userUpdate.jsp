<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>userInfo</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <div class="alert alert-dark" role="alert" align="right">
   	${userInfo.name} さん
   	<a href="userLogout" class="navbar-link logout-link">
    <button type="button" class="btn btn-outline-danger">ログアウト</button>
    </a>
</div>

    <br>
     <div class="text-center">
    <h2>ユーザー情報更新</h2>
    </div>

	<br>

<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

	<br>

<div class="col-10 mx-auto">

<form action="UserUpdate" method="post">
 <input type="hidden" name="id" class="form-control form-control-sm" id="colFormLabelSm" value="${user.id}" readonly >
<div class="form-group row">
      <label for="colFormLabelSm" class="col-sm-6 col-form-label col-form-label-sm">ログインID</label>
    <div class="col-sm-6">
      <input type="text" name="loginId" class="form-control form-control-sm" id="colFormLabelSm" value="${user.loginId}" readonly >
    </div>
  </div>


  <div class="form-group row">
      <label for="colFormLabelSm" class="col-sm-6 col-form-label col-form-label-sm">パスワード</label>
    <div class="col-sm-6">
      <input type="password" name="password" class="form-control form-control-sm" id="colFormLabelSm" >
    </div>
  </div>


  <div class="form-group row">
      <label for="colFormLabelSm" class="col-sm-6 col-form-label col-form-label-sm">パスワード(確認)</label>
    <div class="col-sm-6">
      <input type="password" name="password_again" class="form-control form-control-sm" id="colFormLabelSm" >
    </div>
  </div>


  <div class="form-group row">
      <label for="colFormLabelSm" class="col-sm-6 col-form-label col-form-label-sm">ユーザ名</label>
    <div class="col-sm-6">
      <input type="text" name="name" class="form-control form-control-sm" id="colFormLabelSm" value="${user.name}">
    </div>
  </div>


  <div class="form-group row">
      <label for="colFormLabelSm" class="col-sm-6 col-form-label col-form-label-sm">生年月日</label>
    <div class="col-sm-6">
      <input type="date" name="birthDate" class="form-control form-control-sm" id="colFormLabelSm" value="${user.birthDate}">
    </div>
  </div>

    <br>

    <div align="center">
        <button type="submit" class="btn btn-outline-primary">更新</button>
        </div>
        </form>

	<div>
		<a href="userList" class="navbar-link logout-link">
 			<button type="button" class="btn btn-outline-primary">戻る</button>
		</a>
    </div>
    </div>


</body>
</html>