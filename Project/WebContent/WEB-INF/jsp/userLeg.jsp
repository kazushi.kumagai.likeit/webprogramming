<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>userLeg</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <div class="alert alert-dark" role="alert" align="right">
   	${userInfo.name} さん
   	<a href="userLogout" class="navbar-link logout-link">
    <button type="button" class="btn btn-outline-danger">ログアウト</button>
    </a>
</div>

    <br>
     <div class="text-center">
    <h2>ユーザ新規登録</h2>
    </div>
<br>

<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

	<br>

    <div class="col-10 mx-auto">
    <form action="UserLeg" method="post">
  <div class="form-group row">
      <label for="colFormLabelSm" class="col-sm-6 col-form-label col-form-label-sm">ログインID</label>
    <div class="col-sm-6">
      <input type="text" name="loginId" class="form-control form-control-sm" id="inputLoginId" value="${strLoginId}" >
    </div>
  </div>


  <div class="form-group row">
      <label for="colFormLabelSm" class="col-sm-6 col-form-label col-form-label-sm">パスワード</label>
    <div class="col-sm-6">
      <input type="password" name="password" class="form-control form-control-sm" id="inputPassword" >
    </div>
  </div>


  <div class="form-group row">
      <label for="colFormLabelSm" class="col-sm-6 col-form-label col-form-label-sm">パスワード(確認)</label>
    <div class="col-sm-6">
      <input type="password" name="password_again" class="form-control form-control-sm" id="inputPassword_again" >
    </div>
  </div>


  <div class="form-group row">
      <label for="colFormLabelSm" class="col-sm-6 col-form-label col-form-label-sm">ユーザ名</label>
    <div class="col-sm-6">
      <input type="text" name="name" class="form-control form-control-sm" id="inputName" value="${strName}" >
    </div>
  </div>


  <div class="form-group row">
      <label for="colFormLabelSm" class="col-sm-6 col-form-label col-form-label-sm">生年月日</label>
    <div class="col-sm-6">
      <input type="date" name="birthDate" class="form-control form-control-sm" id="inputBirthDate" value="${strBirthDate}" >
    </div>
  </div>

        <div align="center">
 <button type="submit" class="col-2 btn btn-outline-primary">登録</button>
    </div>

    <br>

	<div>
	<a href="userList" class="navbar-link logout-link">
		<button type="button" class="btn btn-outline-primary">戻る</button>
	</a>
    </div>
    </form>
    </div>

</body>
</html>