
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class UserLeg
 */
@WebServlet("/UserLeg")
public class UserLeg extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserLeg() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");
		if (u == null) {
			response.sendRedirect("userList");
			return;
		}

		//新規登録にフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLeg.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password_again = request.getParameter("password_again");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		UserDao userDao = new UserDao();

		//確認用パスワードが異なる、もしくは入力欄に未入力の項目があった場合
			if (!password.equals(password_again)
				|| loginId.equals("")
				|| password.equals("")
				|| password_again.equals("")
				|| name.equals("")
				|| birthDate.equals("")) {
			// リクエストスコープにエラーメッセージをセット、パスワード以外の入力した値を保存する
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				request.setAttribute("strLoginId", loginId);
				request.setAttribute("strName", name);
				request.setAttribute("strBirthDate", birthDate);
			// ユーザ新規登録画面に戻る
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLeg.jsp");
				dispatcher.forward(request, response);
				return;
			}

		//暗号化処理
			//ハッシュを生成したい元の文字列
			String source = password;
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
			String algorithm = "MD5";

			//ハッシュ生成処理
			byte[] bytes = null;
			try {
				bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			String result = DatatypeConverter.printHexBinary(bytes);
			//標準出力
			System.out.println(result);

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		int user = userDao.Insert(loginId, result, name, birthDate);

		 //登録エラーが発生した場合
		if (user == 0) {
			// リクエストスコープにエラーメッセージをセット、パスワード以外の入力した値を保存する
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				request.setAttribute("strLoginId", loginId);
				request.setAttribute("strName", name);
				request.setAttribute("strBirthDate", birthDate);
			// ユーザ新規登録画面に戻る
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLeg.jsp");
				dispatcher.forward(request, response);
				return;
			}

		//ユーザ一覧画面にフォワード
		response.sendRedirect("userList");

	}
}
