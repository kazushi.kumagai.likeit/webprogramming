

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class UserUpdate
 */
@WebServlet("/UserUpdate")
public class UserUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// TODO：ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		User u =(User)session.getAttribute("userInfo");
		if(u == null) {
			response.sendRedirect("userList");
			return;
		}

		// URLからGETパラメータとしてIDを受け取る
			String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
			System.out.println(id);

		// TODO ：idを引数にして、idに紐づくユーザ情報を出力する
			UserDao userDao = new UserDao();
			User user = userDao.findByUserDetail(id);

		// TODO ：ユーザ情報をリクエストスコープにセットしてjspにフォワード
			request.setAttribute("user", user);

		//ユーザ情報更新フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
        String id = request.getParameter("id");
        String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password_again = request.getParameter("password_again");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		UserDao userDao = new UserDao();

		//確認用パスワードが異なる、もしくは入力欄に未入力の項目があった場合
			if (!password.equals(password_again)
				|| name.equals("")
				|| birthDate.equals("")) {
			// リクエストスコープにエラーメッセージをセット、パスワード以外の入力した値を保存する
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				request.setAttribute("strName", name);
				request.setAttribute("strBirthDate", birthDate);
			// ユーザ新規登録画面に戻る
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLeg.jsp");
				dispatcher.forward(request, response);

		//パスワードと確認用パスワードが未入力の場合
			}else if(password.equals("")
				&& password_again.equals("")){
			//パスワード以外の項目を更新するメソッドを実行
			userDao.Update_notPass(loginId, name, birthDate);
			//ユーザ一覧画面に戻る
			response.sendRedirect("userList");

		//全て入力されている場合
			}else {
			//暗号化処理
				//ハッシュを生成したい元の文字列
					String source = password;
				//ハッシュ生成前にバイト配列に置き換える際のCharset
					Charset charset = StandardCharsets.UTF_8;
				//ハッシュアルゴリズム
					String algorithm = "MD5";
				//ハッシュ生成処理
					byte[] bytes = null;
					try {
						bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
					} catch (NoSuchAlgorithmException e) {
						// TODO 自動生成された catch ブロック
						e.printStackTrace();
					}
					String result = DatatypeConverter.printHexBinary(bytes);
				//標準出力
					System.out.println(result);
			// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
				userDao.Update(loginId,result, name, birthDate);
			//ユーザ一覧画面に戻る
				response.sendRedirect("userList");
		}
	}
}
