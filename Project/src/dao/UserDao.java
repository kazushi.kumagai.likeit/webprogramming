package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.User;

/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao {

    /**
     * ログインIDとパスワードに紐づくユーザ情報を返す
     * @param loginId
     * @param password
     * @return
     */
    public User findByLoginInfo(String loginId, String password) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, password);
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new User(loginIdData, nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }


    /**
     * 全てのユーザ情報を取得する
     * @return
     */
    public List<User> findAll() {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            // TODO: 未実装：管理者以外を取得するようSQLを変更する
            String sql = "SELECT * FROM user WHERE id >= 2";

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }
    //データを追加
    public int Insert(String loginId, String password, String name, String birthDate){
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            //INSERT文を準備
            String sql = "INSERT INTO user (login_id, name, birth_date, password, create_date, update_date)VALUES(?, ?, ?, ?, NOW(), NOW())";

            //INSERTを実行
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1,loginId);
            pStmt.setString(2,name);
            pStmt.setString(3,birthDate);
            pStmt.setString(4,password);

            int i = pStmt.executeUpdate();

            return i;

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
    }

    //idに紐づくデータを取得
    public User findByUserDetail(String Id) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT id, login_id, name, birth_date, create_date, update_date FROM user WHERE id = " + Id;

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            ResultSet rs = pStmt.executeQuery();

            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            int idDate = rs.getInt("id");
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            Date birth_Date = rs.getDate("birth_date");
            String create_Date = rs.getString("create_date");
            String update_Date = rs.getString("update_date");
            //コンストラクタを実行
            return new User(idDate, loginIdData, nameData, birth_Date ,null, create_Date, update_Date);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

  //データを更新
    public int Update(String loginId, String password, String name, String birthDate){
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            //UPDATE文を準備
            String sql = "UPDATE user SET password = ?, name = ?, birth_date = ?, update_date = NOW() WHERE login_id = ?";
            //UPDATEを実行
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1,password);
            pStmt.setString(2,name);
            pStmt.setString(3,birthDate);
            pStmt.setString(4,loginId);

            int i = pStmt.executeUpdate();

            return i;

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
    }

    //パスワード以外のデータを更新
    public int Update_notPass(String loginId, String name, String birthDate){
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            //UPDATE文を準備
            String sql = "UPDATE user SET name = ?, birth_date = ?, update_date = NOW() WHERE login_id = ?";
            //UPDATEを実行
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1,name);
            pStmt.setString(2,birthDate);
            pStmt.setString(3,loginId);

            int i = pStmt.executeUpdate();

            return i;

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
    }

    //データを削除
    public int Delete(String id){
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            //UPDATE文を準備
            String sql = "DELETE FROM user WHERE id = ?";
            //UPDATEを実行
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1,id);

            int i = pStmt.executeUpdate();

            return i;

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
    }
    //ユーザ一覧検索
    public List<User> find(String loginId, String name, String startDate, String endDate) {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE id >= 2";

            if(!loginId.equals("")) {
            	sql += " AND login_id = '" + loginId + "'";
            }

            if(!name.equals("")) {
            	sql += " AND name LIKE '%" + name + "%'";
            }

            if(!startDate.equals("")) {
            	sql += " AND birth_date >= '" + startDate + "'";
            }

            if(!endDate.equals("")) {
            	sql += " AND birth_date <= '" + endDate + "'";
            }

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            ResultSet rs = pStmt.executeQuery();

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId2 = rs.getString("login_id");
                String loginName = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId2, loginName, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }
}
