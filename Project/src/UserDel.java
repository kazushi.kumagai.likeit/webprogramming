

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class UserDel
 */
@WebServlet("/UserDel")
public class UserDel extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDel() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		User u =(User)session.getAttribute("userInfo");
		if(u == null) {
			response.sendRedirect("userList");
			return;
		}

		// URLからGETパラメータとしてIDを受け取る

			String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
			System.out.println(id);

		// TODO ：idを引数にして、idに紐づくユーザ情報を出力する
			UserDao userDao = new UserDao();
			User user = userDao.findByUserDetail(id);

		// TODO ：ユーザ情報をリクエストスコープにセットしてjspにフォワード
			request.setAttribute("user", user);

		//ユーザ削除にフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDel.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        String id = request.getParameter("id");
        UserDao userDao = new UserDao();

        //確認用：削除するidをコンソールに出力
		System.out.println(id);

     // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
     	userDao.Delete(id);

		// ユーザ一覧のjspにフォワード
     	response.sendRedirect("userList");
	}

}
